1. Membuat Database

CREATE DATABASE myshop;

2. Membuat Table di Dalam Database

USE myshop; //Masuk atau terhubung dulu ke database

//Buat table users
CREATE TABLE users(
	id int AUTO_INCREMENT PRIMARY KEY,
    name varchar(255),
    email varchar(255),
    password varchar(255)
);

//Buat table categories
CREATE TABLE categories(
	id int AUTO_INCREMENT PRIMARY KEY,
    name varchar(255)
);

//Buat table items
CREATE TABLE items(
    id int AUTO_INCREMENT PRIMARY KEY,
    name varchar(255),
    description varchar(255),
    price int,
    stock int,
    category_id int not null,
    FOREIGN KEY (category_id) REFERENCES categories(id)
);


3. Memasukkan Data pada Table

//Isi data table users
INSERT INTO users (name, email, password)
VALUES
    	('John Doe', 'john@doe.com','john123'),
        ('Jane Doe', 'jone@doe.com','jenita123');

//Isi data table categories
INSERT INTO categories (name)
VALUES
    	('gadget'),
        ('cloth'),
        ('men'),
        ('women'),
        ('branded');

//Isi data table item
INSERT INTO items
		(name, description, price, stock, category_id)
	VALUES
    	('Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100,1),
	('Uniklooh', 'baju keren dari brand ternama', 500000, 50,2),
        ('IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10,1);

4. Mengambil Data dari Database

a. Mengambil data users

SELECT name, email from users;

b. Mengambil data items

SELECT * from items WHERE price > 1000000;

SELECT * from items WHERE  name like '%watch%';

c. Menampilkan data item join dengan kategori

SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name
FROM items
INNER JOIN categories ON items.category_id=categories.id;

5. Mengubah Data dari Database

UPDATE items
SET price = 2500000
WHERE name='Sumsang b50';